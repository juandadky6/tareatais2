package com.dadky.barberbookingapp.Model;

public class Historial {

    private String id;
    private String date;
    private String precio;

    public Historial() {
    }

    public Historial(String id, String date, String precio) {
        this.id = id;
        this.date = date;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
}
