package com.dadky.barberbookingapp.Model;

public class Banner {

    //Lookbook y banner son lo mismo
    private String image;

    public Banner() {
    }

    public Banner(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
