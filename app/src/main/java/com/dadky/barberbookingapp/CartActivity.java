package com.dadky.barberbookingapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.TextView;

import com.dadky.barberbookingapp.Adapter.CartAdapter;
import com.dadky.barberbookingapp.Database.CartDatabase;
import com.dadky.barberbookingapp.Database.CartItem;
import com.dadky.barberbookingapp.Database.DatabaseUtils;
import com.dadky.barberbookingapp.Interface.ICartItemLoadListener;
import com.dadky.barberbookingapp.Interface.ICartItemUpdateListener;
import com.dadky.barberbookingapp.Interface.ISumCartListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dmax.dialog.SpotsDialog;

public class CartActivity extends AppCompatActivity implements ICartItemLoadListener, ICartItemUpdateListener, ISumCartListener {

    @BindView(R.id.recycler_cart)
    RecyclerView recycler_cart;
    @BindView(R.id.txt_total_price)
    TextView txt_total_price;
    @BindView(R.id.btn_submit_cart)
    Button btn_submit_cart;

    private AlertDialog dialog;

    @OnClick(R.id.btn_submit_cart)
    void submitClick(){
        comprarProducto();
    }

    private void comprarProducto() {
        startActivity(new Intent(this,BuyActivity.class));
    }

    CartDatabase cartDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        ButterKnife.bind(CartActivity.this);

        cartDatabase = CartDatabase.getInstance(this);

        DatabaseUtils.getAllCart(cartDatabase,this);

        dialog = new SpotsDialog.Builder().setMessage("Cargando...").setCancelable(false).setContext(this).build();
        dialog.show();

        //Vista
        recycler_cart.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recycler_cart.setLayoutManager(linearLayoutManager);
        recycler_cart.addItemDecoration(new DividerItemDecoration(this,linearLayoutManager.getOrientation()));

        onCartItemUpdateSuccess();
    }

    @Override
    public void onGetAllItemFromCartSuccess(List<CartItem> cartItemList) {
        //Aqui,despues de obtener cartitems de db
        //Se mostrara por RecyclerView
        CartAdapter adapter = new CartAdapter(this,cartItemList,this);
        recycler_cart.setAdapter(adapter);
        dialog.dismiss();
    }

    @Override
    public void onCartItemUpdateSuccess() {
        DatabaseUtils.sumCart(cartDatabase,this);
    }

    @Override
    public void onSumCartSuccess(Long value) {
        txt_total_price.setText(new StringBuilder("$").append(value));
    }
}
