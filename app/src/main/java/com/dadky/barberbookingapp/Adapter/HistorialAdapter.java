package com.dadky.barberbookingapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dadky.barberbookingapp.Model.Historial;
import com.dadky.barberbookingapp.R;

import org.w3c.dom.Text;

import java.util.List;

public class HistorialAdapter extends RecyclerView.Adapter<HistorialAdapter.ViewHolder> {

    List<Historial> historials;
    Context context;

    public HistorialAdapter(List<Historial> historials, Context context) {
        this.historials = historials;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_historial,viewGroup,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtId.setText("Precio:" + historials.get(i).getId());
        viewHolder.txtfecha.setText("Fecha:" + historials.get(i).getDate());
        viewHolder.txtprecio.setText("Precio: S/." + historials.get(i).getPrecio());
    }

    @Override
    public int getItemCount() {
        return historials.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtId;
        TextView txtfecha;
        TextView txtprecio;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtId = itemView.findViewById(R.id.id_compra);
            txtfecha = itemView.findViewById(R.id.id_fecha);
            txtprecio = itemView.findViewById(R.id.id_precio);
        }
    }
}
