package com.dadky.barberbookingapp.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.dadky.barberbookingapp.Fragments.BuyStep1Fragment;
import com.dadky.barberbookingapp.Fragments.BuyStep2Fragment;
import com.dadky.barberbookingapp.Fragments.BuyStep3Fragment;

public class MyViewPagerAdapter extends FragmentPagerAdapter {

    public MyViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                return BuyStep1Fragment.getInstance();
            case 1:
                return BuyStep2Fragment.getInstance();
            case 2:
                return BuyStep3Fragment.getInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
