package com.dadky.barberbookingapp.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dadky.barberbookingapp.Common.Common;
import com.dadky.barberbookingapp.Database.CartDatabase;
import com.dadky.barberbookingapp.Database.CartItem;
import com.dadky.barberbookingapp.Database.DatabaseUtils;
import com.dadky.barberbookingapp.HomeActivity;
import com.dadky.barberbookingapp.Interface.IRecyclerItemSelectedListener;
import com.dadky.barberbookingapp.MainActivity;
import com.dadky.barberbookingapp.Model.ShoppingItem;
import com.dadky.barberbookingapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ShoppingItemAdapter extends RecyclerView.Adapter<ShoppingItemAdapter.ViewHolder> {

    Context context;
    List<ShoppingItem> shoppingItemList;
    CartDatabase cartDatabase;

    public ShoppingItemAdapter(Context context, List<ShoppingItem> shoppingItemList) {
        this.context = context;
        this.shoppingItemList = shoppingItemList;
        cartDatabase = CartDatabase.getInstance(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_shopping_item, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        Picasso.get().load(shoppingItemList.get(i).getImage()).into(viewHolder.img_shopping_item);
        viewHolder.txt_shopping_item_name.setText(Common.formatShoppingItemName(shoppingItemList.get(i).getName()));
        viewHolder.txt_shopping_item_price.setText(new StringBuilder("S/.").append(shoppingItemList.get(i).getPrice()));

        //Añadir a carrito
        viewHolder.setiRecyclerItemSelectedListener(new IRecyclerItemSelectedListener() {
            @Override
            public void onItemSelectedListener(View view, int pos) {
                //Crear cartitem
                if(Common.currentUser != null) {
                    CartItem cartItem = new CartItem();
                    cartItem.setProductId(shoppingItemList.get(pos).getId());
                    cartItem.setProductName(shoppingItemList.get(pos).getName());
                    cartItem.setProductImage(shoppingItemList.get(pos).getImage());
                    cartItem.setProductQuantity(1);
                    cartItem.setProductPrice(shoppingItemList.get(pos).getPrice());
                    cartItem.setUserPhone(Common.currentUser.getPhoneNumber());

                    //Insert a la db
                    DatabaseUtils.insertToCart(cartDatabase, cartItem);
                    Toast.makeText(context, "Añadido al carrito!", Toast.LENGTH_SHORT).show();
                }else{
                    AlertDialog.Builder dialog = new AlertDialog.Builder(context)
                            .setTitle("Inicie Sesion")
                            .setMessage("Debe iniciar sesión para poder realizar las funciones disponibles. ¿Desea iniciar sesión?")
                            .setCancelable(false)
                            .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(context, MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.getApplicationContext().startActivity(intent);
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    dialog.show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return shoppingItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_shopping_item_name, txt_shopping_item_price, txt_add_to_cart;
        ImageView img_shopping_item;

        IRecyclerItemSelectedListener iRecyclerItemSelectedListener;

        public void setiRecyclerItemSelectedListener(IRecyclerItemSelectedListener iRecyclerItemSelectedListener) {
            this.iRecyclerItemSelectedListener = iRecyclerItemSelectedListener;
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            img_shopping_item = itemView.findViewById(R.id.img_shopping_item);
            txt_shopping_item_name = itemView.findViewById(R.id.txt_name_shopping_item);
            txt_shopping_item_price = itemView.findViewById(R.id.txt_price_shopping_item);
            txt_add_to_cart = itemView.findViewById(R.id.txt_add_to_cart);

            txt_add_to_cart.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            iRecyclerItemSelectedListener.onItemSelectedListener(v, getAdapterPosition());
        }
    }
}
