package com.dadky.barberbookingapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dadky.barberbookingapp.Database.CartDatabase;
import com.dadky.barberbookingapp.Database.CartItem;
import com.dadky.barberbookingapp.Database.DatabaseUtils;
import com.dadky.barberbookingapp.Interface.ICartItemUpdateListener;
import com.dadky.barberbookingapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {

    Context context;
    private List<CartItem> cartItemList;
    CartDatabase cartDatabase;
    ICartItemUpdateListener iCartItemUpdateListener;

    public CartAdapter(Context context, List<CartItem> cartItemList, ICartItemUpdateListener icartItemUpdateListener) {
        this.context = context;
        this.cartItemList = cartItemList;
        cartDatabase = CartDatabase.getInstance(context);
        this.iCartItemUpdateListener = icartItemUpdateListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.layout_cart_item,viewGroup,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        Picasso.get().load(cartItemList.get(i).getProductImage()).into(viewHolder.img_product);
        viewHolder.txt_cart_name.setText(new StringBuilder(cartItemList.get(i).getProductName()));
        viewHolder.txt_cart_price.setText(new StringBuilder("$").append(cartItemList.get(i).getProductPrice()));
        viewHolder.txt_quantity.setText(new StringBuilder(String.valueOf(cartItemList.get(i).getProductQuantity())));

        //Evento
        viewHolder.setListener(new IImageButtonListener() {
            @Override
            public void onImageButtonClick(View view, int pos, boolean isDecrease) {
                if(isDecrease){
                    if(cartItemList.get(pos).getProductQuantity() > 0){
                        cartItemList.get(pos).setProductQuantity(cartItemList.get(pos).getProductQuantity()-1);
                        DatabaseUtils.updateCart(cartDatabase,cartItemList.get(pos));
                    }
                    viewHolder.txt_quantity.setText(new StringBuilder(String.valueOf(cartItemList.get(pos).getProductQuantity())));
                    iCartItemUpdateListener.onCartItemUpdateSuccess();
                }else{
                    if(cartItemList.get(pos).getProductQuantity() < 99){
                        cartItemList.get(pos).setProductQuantity(cartItemList.get(pos).getProductQuantity()+1);
                        DatabaseUtils.updateCart(cartDatabase,cartItemList.get(pos));
                    }
                    viewHolder.txt_quantity.setText(new StringBuilder(String.valueOf(cartItemList.get(pos).getProductQuantity())));
                    iCartItemUpdateListener.onCartItemUpdateSuccess();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return cartItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txt_cart_name,txt_cart_price,txt_quantity;
        ImageView img_decrease,img_increase,img_product;

        IImageButtonListener listener;

        public void setListener(IImageButtonListener listener) {
            this.listener = listener;
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_cart_name = itemView.findViewById(R.id.txt_cart_name);
            txt_cart_price = itemView.findViewById(R.id.txt_cart_price);
            txt_quantity = itemView.findViewById(R.id.txt_cart_quantity);

            img_decrease = itemView.findViewById(R.id.img_decrease);
            img_increase = itemView.findViewById(R.id.img_increase);
            img_product = itemView.findViewById(R.id.cart_img);

            //Event
            img_decrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onImageButtonClick(v,getAdapterPosition(),true);
                }
            });

            img_increase.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onImageButtonClick(v,getAdapterPosition(),false);
                }
            });
        }
    }

    interface IImageButtonListener{
        void onImageButtonClick(View view,int pos,boolean isDecrease);
    }

}
