package com.dadky.barberbookingapp;

import android.app.AlertDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.dadky.barberbookingapp.Adapter.HistorialAdapter;
import com.dadky.barberbookingapp.Common.Common;
import com.dadky.barberbookingapp.Common.SpacesItemDecoration;
import com.dadky.barberbookingapp.Interface.IHistorialLoadListener;
import com.dadky.barberbookingapp.Model.Historial;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import dmax.dialog.SpotsDialog;

public class HistoryActivity extends AppCompatActivity implements IHistorialLoadListener {

    CollectionReference purchasesRef;
    AlertDialog dialog;
    HistorialAdapter adapter;

    @BindView(R.id.recycler_historial)
    RecyclerView recycler_historial;
    @BindView(R.id.txt_vacio)
    TextView txt_vacio;
    IHistorialLoadListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        ButterKnife.bind(HistoryActivity.this);

        txt_vacio.setVisibility(View.GONE);

        ///User/+51950600687/Purchases
        dialog = new SpotsDialog.Builder().setContext(this).setCancelable(false).setMessage("Cargando...").build();

        initView();

        if(Common.currentUser != null) {
            purchasesRef = FirebaseFirestore.getInstance()
                    .collection("User")
                    .document(Common.currentUser.getPhoneNumber())
                    .collection("Purchases");

            purchasesRef.get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            ArrayList<Historial> list = new ArrayList<>();
                            for (QueryDocumentSnapshot historialSnapShot:task.getResult()){
                                Historial historial = historialSnapShot.toObject(Historial.class);
                                historial.setId(historialSnapShot.getId());
                                list.add(historial);
                                dialog.dismiss();
                                onHistorialLoadSuccess(list);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            dialog.dismiss();
                            onHistorialLoadFailed(e.getMessage());
                            txt_vacio.setVisibility(View.VISIBLE);
                        }
                    });
        }
    }

    private void initView() {
        recycler_historial.setHasFixedSize(true);
        recycler_historial.setLayoutManager(new LinearLayoutManager(this));
        recycler_historial.addItemDecoration(new SpacesItemDecoration(4));
    }

    @Override
    public void onHistorialLoadSuccess(List<Historial> historial) {
        if(historial.size()>0) {
            adapter = new HistorialAdapter(historial, this);
            recycler_historial.setAdapter(adapter);
        }else{
            txt_vacio.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onHistorialLoadFailed(String message) {
        Toast.makeText(this, ""+message, Toast.LENGTH_SHORT).show();
    }
}
