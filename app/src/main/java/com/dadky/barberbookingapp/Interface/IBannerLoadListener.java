package com.dadky.barberbookingapp.Interface;

import com.dadky.barberbookingapp.Model.Banner;

import java.util.List;

public interface IBannerLoadListener {
    void onBannerLoadSuccess(List<Banner> banners);
    void onBannerLoadFailed(String message);
}
