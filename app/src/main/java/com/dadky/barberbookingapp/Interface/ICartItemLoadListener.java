package com.dadky.barberbookingapp.Interface;

import com.dadky.barberbookingapp.Database.CartItem;

import java.util.List;

public interface ICartItemLoadListener {
    void onGetAllItemFromCartSuccess(List<CartItem> cartItemList);
}
