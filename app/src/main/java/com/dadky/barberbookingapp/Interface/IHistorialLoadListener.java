package com.dadky.barberbookingapp.Interface;

import com.dadky.barberbookingapp.Model.Historial;

import java.util.List;

public interface IHistorialLoadListener {
    void onHistorialLoadSuccess(List<Historial> historial);
    void onHistorialLoadFailed(String message);
}
