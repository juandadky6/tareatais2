package com.dadky.barberbookingapp.Interface;

public interface ICountItemInCartListener {
    void onCartItemCountSuccess(int count);
}
