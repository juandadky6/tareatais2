package com.dadky.barberbookingapp.Interface;

public interface ICartItemUpdateListener {
    void onCartItemUpdateSuccess();
}
