package com.dadky.barberbookingapp.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.chip.Chip;
import android.support.design.chip.ChipGroup;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dadky.barberbookingapp.Adapter.ShoppingItemAdapter;
import com.dadky.barberbookingapp.Common.SpacesItemDecoration;
import com.dadky.barberbookingapp.Interface.IShoppingDataLoadListener;
import com.dadky.barberbookingapp.Model.ShoppingItem;
import com.dadky.barberbookingapp.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShoppingFragment extends Fragment implements IShoppingDataLoadListener {

    CollectionReference shoppingitemRef;

    IShoppingDataLoadListener iShoppingDataLoadListener;

    Unbinder unbinder;

    @BindView(R.id.recycler_items)
    RecyclerView recycler_items;

    @BindView(R.id.chip_group)
    ChipGroup chipGroup;
    @BindView(R.id.chip_bebida)
    Chip chip_bebida;

    @OnClick(R.id.chip_bebida)
    void bebidaChipClick(){
        setSelectedChip(chip_bebida);
        loadShoppingItem("Bebidas");
    }

    @BindView(R.id.chip_Chocolate)
    Chip chip_chocolate;

    @OnClick(R.id.chip_Chocolate)
    void chocolateChipClick(){
        setSelectedChip(chip_chocolate);
        loadShoppingItem("Chocolates");
    }

    @BindView(R.id.chip_chicle_dulce)
    Chip chip_chicle_dulce;

    @OnClick(R.id.chip_chicle_dulce)
    void chicleDulceChipClick(){
        setSelectedChip(chip_chicle_dulce);
        loadShoppingItem("Chicles y dulces");
    }

    @BindView(R.id.chip_galleta)
    Chip chip_galleta;

    @OnClick(R.id.chip_galleta)
    void galletaChipClick(){
        setSelectedChip(chip_galleta);
        loadShoppingItem("Galletas");
    }

    @BindView(R.id.chip_licor)
    Chip chip_licor;

    @OnClick(R.id.chip_licor)
    void licorChipClick(){
        setSelectedChip(chip_licor);
        loadShoppingItem("Licor");
    }

    @BindView(R.id.chip_snack)
    Chip chip_snack;

    @OnClick(R.id.chip_snack)
    void snackChipClick(){
        setSelectedChip(chip_snack);
        loadShoppingItem("Snacks");
    }

    @BindView(R.id.chip_jugo_energizante)
    Chip chip_jugo_energizante;

    @OnClick(R.id.chip_jugo_energizante)
    void jugoEnergizanteChipClick(){
        setSelectedChip(chip_jugo_energizante);
        loadShoppingItem("Jugos y energizantes");
    }

    @BindView(R.id.chip_lacteo)
    Chip chip_lacteo;

    @OnClick(R.id.chip_lacteo)
    void lacteoChipClick(){
        setSelectedChip(chip_lacteo);
        loadShoppingItem("Lacteos");
    }

    @BindView(R.id.chip_medicina)
    Chip chip_medicina;

    @OnClick(R.id.chip_medicina)
    void medicinaChipClick(){
        setSelectedChip(chip_medicina);
        loadShoppingItem("Medicina");
    }

    @BindView(R.id.chip_articulo_aseo)
    Chip chip_articulo_aseo;

    @OnClick(R.id.chip_articulo_aseo)
    void articuloAseoChipClick(){
        setSelectedChip(chip_articulo_aseo);
        loadShoppingItem("Articulos de aseo");
    }

    private void loadShoppingItem(String itemMenu) {
        shoppingitemRef = FirebaseFirestore.getInstance()
                .collection("Shopping")
                .document(itemMenu)
                .collection("Items");

        //Obtener data
        shoppingitemRef.get()
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        iShoppingDataLoadListener.onShoppingDataLoadFailed(e.getMessage());
                    }
                }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    List<ShoppingItem> shoppingItems = new ArrayList<>();
                    for(DocumentSnapshot itemSnapShot:task.getResult()){
                        ShoppingItem shoppingItem = itemSnapShot.toObject(ShoppingItem.class);
                        shoppingItem.setId(itemSnapShot.getId()); //Recordar añadir si no se desea obtener una referencia nula
                        shoppingItems.add(shoppingItem);
                    }
                    iShoppingDataLoadListener.onShoppingDataLoadSuccess(shoppingItems);
                }
            }
        });
    }

    private void setSelectedChip(Chip chip) {
        //Dar color
        for(int i = 0;i<chipGroup.getChildCount();i++){
            Chip chipItem = (Chip) chipGroup.getChildAt(i);
            if(chipItem.getId() != chip.getId()) { //No esta seleccionado

                chipItem.setChipBackgroundColorResource(android.R.color.darker_gray);
                chipItem.setTextColor(getResources().getColor(android.R.color.white));

            }else{  //seleccionado

                chipItem.setChipBackgroundColorResource(android.R.color.holo_orange_dark);
                chipItem.setTextColor(getResources().getColor(android.R.color.black));

            }
        }
    }



    public ShoppingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View itemView = inflater.inflate(R.layout.fragment_shopping, container, false);

        unbinder = ButterKnife.bind(this,itemView);

        iShoppingDataLoadListener = this;

        //Carga por defecto
        loadShoppingItem("Bebidas");

        initView();

        return itemView;
    }

    private void initView() {
        recycler_items.setHasFixedSize(true);
        recycler_items.setLayoutManager(new GridLayoutManager(getContext(),2));
        recycler_items.addItemDecoration(new SpacesItemDecoration(8));
    }

    @Override
    public void onShoppingDataLoadSuccess(List<ShoppingItem> shoppingItemList) {
        ShoppingItemAdapter adapter = new ShoppingItemAdapter(getContext(),shoppingItemList);
        recycler_items.setAdapter(adapter);
    }

    @Override
    public void onShoppingDataLoadFailed(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
