package com.dadky.barberbookingapp.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.dadky.barberbookingapp.Common.Common;
import com.dadky.barberbookingapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BuyStep1Fragment extends Fragment {

    private static BuyStep1Fragment instance;

    Unbinder unbinder;

    LocalBroadcastManager localBroadcastManager;


    @OnClick(R.id.img_visa)
    void clickVisa(){
        Intent intent = new Intent(Common.KEY_ENABLE_BUTTON_NEXT);
        intent.putExtra(Common.KEY_PAGO_STORE,"Visa");
        localBroadcastManager.sendBroadcast(intent);
    }

    @OnClick(R.id.img_mastercard)
    void clickMastercard(){
        Intent intent = new Intent(Common.KEY_ENABLE_BUTTON_NEXT);
        intent.putExtra(Common.KEY_PAGO_STORE,"MasterCard");
        localBroadcastManager.sendBroadcast(intent);
    }

    public static BuyStep1Fragment getInstance(){
        if(instance == null)
            instance = new BuyStep1Fragment();
        return instance;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View itemView = inflater.inflate(R.layout.fragment_buy_step_one,container,false);
        unbinder = ButterKnife.bind(this,itemView);
        localBroadcastManager = LocalBroadcastManager.getInstance(getActivity());

        return itemView;
    }
}
